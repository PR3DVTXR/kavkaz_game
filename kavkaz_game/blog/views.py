from django.shortcuts import render
from .models import Blog


def all_blogs(request):
    blog = Blog.objects.all()[:6]
    return render(request, 'blog/all_blogs.html', {'blog':blog})

def view_blog(request):
    pass