from django.db import models


class Blog(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField()
    img = models.ImageField(upload_to='blog/images')
    date = models.DateField()
    game = 'gm'
    life = 'lf'
    advises = 'ad'
    update = 'up'
    other = 'ot'

    THEME_CHOICES = [
        (game, 'game'),
        (life, 'life'),
        (advises, 'advises'),
        (update, 'update'),
        (other, 'other'),
    ]
    theme = models.CharField(max_length=50, choices=THEME_CHOICES, default=THEME_CHOICES[0])


    def __str__(self):
        return self.title