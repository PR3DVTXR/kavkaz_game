from django.urls import path
from . import views


urlpatterns = [
    path('all_blogs/', views.all_blogs, name='all_blogs'),
    # path('all_blogs/<int:blog_id>', views.view_blog, name='view_blog'),
]