from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings
from django.urls import path, include
from game import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('auth/', include('authentification.urls')),
    path('blog/', include('blog.urls')),
    path('home/', views.home, name='home'),
    path('all_games/', views.all_games, name='all_games'),
    path('<int:game_id>/', views.view_game, name='view_game'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)