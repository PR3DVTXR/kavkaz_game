from django.urls import path
from . import views


urlpatterns = [
    path('sign_in/', views.signinuser, name='signinuser'),
    path('log_in/', views.loginuser, name='loginuser'),
    path('log_out/', views.logoutuser, name='logoutuser'),
]