from django.shortcuts import render, get_object_or_404
from .models import Game


def home(request):
    return render(request, 'game/home.html')


def all_games(request):
    game = Game.objects.all()
    return render(request, 'game/all_games.html', {'game':game})


def view_game(request, game_id):
    game = get_object_or_404(Game, pk=game_id)
    return render(request, 'game/views_games.html', {'id':game})