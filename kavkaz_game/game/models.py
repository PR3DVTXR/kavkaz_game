from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse


class Game(models.Model):

    class NewManager(models.Model):
        def get_queryset(self):
            return super().get_queryset()

    title = models.CharField(max_length=25)
    description = models.TextField()
    img1 = models.ImageField(upload_to='games/images')
    img2 = models.ImageField(upload_to='games/images', blank=True)
    size = models.CharField(max_length=6)
    favorites = models.ManyToManyField(User, related_name='favorite', default=None, blank=True)
    baskets = models.ManyToManyField(User, related_name='baskets', default=None, blank=True)


    objects = models.Manager()
    newmanager = NewManager()

    def __str__(self):
        return self.title